/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi16;

import java.lang.reflect.Array;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lucy
 */
public class CalcularPersonasTest {
    
    public CalcularPersonasTest() {
    }
    
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    static CalcularPersonas instance;
    @AfterClass
    public static void tearDownClass() {
        System.out.println("AfterClass");
        instance = new CalcularPersonas(); 
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calcularPersonasTiempoParaArreglos method, of class CalcularPersonas.
     */
    @Test
    public void testCalcularPersonasTiempoParaArreglos() {
        System.out.println("calcularPersonasTiempoParaArreglos");
        int n = 0;
        //CalcularPersonas instance = new CalcularPersonas();
        int[] expResult = new int[n];
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    //Test para la cantidad de arreglos de 19
    @Test
    public void testCalcularPersonasTiempoPara19Arreglos() {
        System.out.println("testCalcularPersonasTiempoPara19Arreglos");
        int n = 19;
        //CalcularPersonas instance = new CalcularPersonas();
        int[] expResult = new int[n];
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        //assertArrayEquals(expResult, result);
        assertEquals("Número incorrecto de personas",2,result.length);
        Assert.assertArrayEquals(expResult, result);
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testCalcularPersonasTiempoPara49Arreglos() {
        System.out.println("testCalcularPersonasTiempoPara49Arreglos");
        int n = 49;
        //CalcularPersonas instance = new CalcularPersonas();
        int[] expResult = null;
        try{
            int[] result = instance.calcularPersonasTiempoParaArreglos(n);
            fail("Se esperaba una excepción");
        }catch(Exception ex){
        }
    }
    
     @Test
    public void testCalcularPersonasTiempoPara50Arreglos() {
        System.out.println("testCalcularPersonasTiempoPara50Arreglos");
        int n = 49;
        //CalcularPersonas instance = new CalcularPersonas();
        int[] expResult = null;
        try{
            int[] result = instance.calcularPersonasTiempoParaArreglos(n);
            fail("Se esperaba una excepción");
        }catch(Exception ex){
        }
    }
    
    @Test
    public void testCalcularPersonasTiempoPara0Arreglos() {
        System.out.println("testCalcularPersonasTiempoPara0Arreglos");
        int n = 0;
        //CalcularPersonas instance = new CalcularPersonas();
        int[] expResult = new int[n];
       int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertEquals("El número de personas debe de ser ninguna",0,result.length);

        }
}
