/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi16.ranchoganadero.Controllers;

import java.util.List;
import mx.lania.rysi16.ranchoganadero.entidades.Proveedor;
import mx.lania.rysi16.ranchoganadero.oad.oadProveedor;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Lucy
 */
@RestController
@RequestMapping("/proveedor")
public class ProveedorController {
    private static org.slf4j.Logger Logger = LoggerFactory.getLogger(ProveedorController.class);
    
    @Autowired
    oadProveedor oadProveedor;
   
    @RequestMapping("/proveedor") //Metodo get que viene por default por eso no se pone
   // @GetMapping("/animal") //Metodo get que viene por default por eso no se pone
    public List<Proveedor> getProveedor(){
        Logger.debug("GET /proveedor");
        return oadProveedor.findAll();
    }
    

   @RequestMapping(value="",method=RequestMethod.POST)
    public Proveedor guardarProveedor(@RequestBody Proveedor proveedor){
        Logger.debug("POST /proveedor {}",proveedor);
        oadProveedor.save(proveedor);
        return  proveedor;
    }
    
      @RequestMapping(value="/{id}",method=RequestMethod.PUT)
    public Proveedor actualizarProveedor(@RequestBody Proveedor proveedor){
        Logger.debug("PUT /proveedor/{} {}",proveedor.getIdOrden(), proveedor);
        oadProveedor.save(proveedor);
        return proveedor;
    }
    
    @RequestMapping(value="/{id}",method=RequestMethod.DELETE)
    public Proveedor borrarProveedor(@PathVariable("id") Integer id){
        Logger.debug("DELETE /proveedor {}",id);
        oadProveedor.delete(id);
        return new Proveedor(id);
    }
}
