/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi16.ranchoganadero.oad;

import java.util.List;
import mx.lania.rysi16.ranchoganadero.entidades.Corrales;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Lucy
 */
public interface oadCorrales extends JpaRepository<Corrales, Integer>{
    void insertar(Corrales animal);
    void eliminar(Integer id);
    void editar(Integer id);
    Corrales showAnimal(Integer id);
    List<Corrales> listAnimal();
    List<Corrales> buscarActivosPorNombre(String nombre);

    List<Corrales> buscarPorNombre(String nombre);
    
    List<Corrales> getActivos();
    
    Corrales getPorId(Integer id);
}
