/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi16.ranchoganadero.oad;

import java.util.List;
import mx.lania.rysi16.ranchoganadero.entidades.Proveedor;
import mx.lania.rysi16.ranchoganadero.entidades.Raza;

/**
 *
 * @author Lucy
 */
public interface oadProveedor {
    void editar(Integer id);
    Proveedor showProveedor(Integer id);
    List<Proveedor> listProveedor();
    
    List<Proveedor> buscarActivosPorNombre(String nombre);

    List<Proveedor> buscarPorNombre(String nombre);
    
    List<Proveedor> getActivos();
    
    Proveedor getPorId(Integer id);

    public List<Proveedor> findAll();

    public void save(Proveedor proveedor);

    public void delete(Integer id);
}
