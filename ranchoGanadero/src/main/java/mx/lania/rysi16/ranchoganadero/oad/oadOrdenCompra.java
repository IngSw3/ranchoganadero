/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi16.ranchoganadero.oad;

import java.util.List;
import mx.lania.rysi16.ranchoganadero.entidades.OrdenCompra;

/**
 *
 * @author Lucy
 */
public interface oadOrdenCompra {
   
    OrdenCompra showOrdenCompra(Integer id);
    List<OrdenCompra> listOrdenCompra();
    List<OrdenCompra> buscarActivosPorNombre(String nombre);

    List<OrdenCompra> buscarPorNombre(String nombre);
    
    List<OrdenCompra> getActivos();
    
    OrdenCompra getPorId(Integer id);

    public void delete(Integer id);

    public void save(OrdenCompra orden);

    public List<OrdenCompra> findAll();
}
