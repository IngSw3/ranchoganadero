/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi16.ranchoganadero.oad;

import java.util.List;
import mx.lania.rysi16.ranchoganadero.entidades.Animal;
import org.slf4j.Logger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Lucy
 */
public interface oadAnimal extends JpaRepository<Animal, Integer>{
    @Query("SELECT e FROM Animal e WHERE e.animal.idAnimal = :idAnimal")
    List<Animal> getPorIdAnimal(@Param("idAnimal") Integer idAnimal);

    public List<Animal> findByClave(String clave);
    
     
    
}
