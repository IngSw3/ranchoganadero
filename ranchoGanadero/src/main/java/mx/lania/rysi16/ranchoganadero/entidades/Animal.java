/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi16.ranchoganadero.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Lucy
 */
@Entity
@Table(name = "animal")
public class Animal implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_animal")
    private Integer id;
    @Column(name = "clave")
    private String clave;
    @Column(name = "id_raza")
    private Integer id_raza;
    @Column(name = "nombre")
    private String nombre;

    public Animal() {
    }

    public Animal(Integer idAnimal) {
        this.id = idAnimal;
    }

    public Integer getIdAnimal() {
        return id;
    }

    public void setIdAnimal(Integer id) {
        this.id = id;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String nombre) {
        this.clave = clave;
    }

     public Integer getIdRaza() {
        return id_raza;
    }

    public void setIdRaza(Integer id_raza) {
        this.id_raza = id_raza;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Animal)) {
            return false;
        }
        Animal other = (Animal) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.lania.rysi16.ranchoganadero.entidades.Animal[ idAnimal=" + id + " ]";
    }
    
}
