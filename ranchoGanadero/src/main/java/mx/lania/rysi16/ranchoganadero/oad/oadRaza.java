/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi16.ranchoganadero.oad;

import java.util.List;
import mx.lania.rysi16.ranchoganadero.entidades.Raza;

/**
 *
 * @author Lucy
 */
public interface oadRaza {
    void insertar(Raza raza);
    void eliminar(Integer id);
    void editar(Integer id);
    Raza showRaza(Integer id);
    List<Raza> listRaza();
    
    List<Raza> buscarActivosPorNombre(String nombre);

    List<Raza> buscarPorNombre(String nombre);
    
    List<Raza> getActivos();
    
    Raza getPorId(Integer id);

    public List<Raza> findAll();

    public void save(Raza raza);

    public void delete(Integer id);
}
