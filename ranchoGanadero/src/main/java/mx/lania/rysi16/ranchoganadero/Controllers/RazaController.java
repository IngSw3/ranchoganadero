/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi16.ranchoganadero.Controllers;

import java.util.List;
import mx.lania.rysi16.ranchoganadero.entidades.Raza;

import mx.lania.rysi16.ranchoganadero.oad.oadRaza;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Lucy
 */
@RestController
@RequestMapping("/raza")
public class RazaController {
    private static org.slf4j.Logger Logger = LoggerFactory.getLogger(RazaController.class);
    
    @Autowired
    oadRaza oadRaza;
   
    @RequestMapping("/raza") //Metodo get que viene por default por eso no se pone
   // @GetMapping("/animal") //Metodo get que viene por default por eso no se pone
    public List<Raza> getRaza(){
        Logger.debug("GET /raza");
        return oadRaza.findAll();
    }
    

   @RequestMapping(value="",method=RequestMethod.POST)
    public Raza guardarRaza(@RequestBody Raza raza){
        Logger.debug("POST /raza {}",raza);
        oadRaza.save(raza);
        return  raza;
    }
    
      @RequestMapping(value="/{id}",method=RequestMethod.PUT)
    public Raza actualizarRaza(@RequestBody Raza raza){
        Logger.debug("PUT /ordenes/{} {}",raza.getIdRaza(), raza);
        oadRaza.save(raza);
        return raza;
    }
    
    @RequestMapping(value="/{id}",method=RequestMethod.DELETE)
    public Raza borrarRaza(@PathVariable("id") Integer id){
        Logger.debug("DELETE /raza {}",id);
        oadRaza.delete(id);
        return new Raza(id);
    }
}
