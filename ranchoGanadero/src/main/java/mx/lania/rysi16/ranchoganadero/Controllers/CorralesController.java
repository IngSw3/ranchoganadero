/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi16.ranchoganadero.Controllers;

import java.util.List;
import mx.lania.rysi16.ranchoganadero.entidades.Corrales;
import mx.lania.rysi16.ranchoganadero.oad.oadCorrales;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Lucy
 */
@RestController
@RequestMapping("/corral")
public class CorralesController {
    private static org.slf4j.Logger Logger = LoggerFactory.getLogger(CorralesController.class);
    
    @Autowired
    oadCorrales oadCorrales;
   
    @RequestMapping("/corral") //Metodo get que viene por default por eso no se pone
   // @GetMapping("/animal") //Metodo get que viene por default por eso no se pone
    public List<Corrales> getCorral(){
        Logger.debug("GET /corrales");
        return oadCorrales.findAll();
    }
    

   @RequestMapping(value="",method=RequestMethod.POST)
    public Corrales guardarCorral(@RequestBody Corrales corral){
        Logger.debug("POST /corrales {}",corral);
        oadCorrales.save(corral);
        return  corral;
    }
    
      @RequestMapping(value="/{id}",method=RequestMethod.PUT)
    public Corrales actualizarCorral(@RequestBody Corrales corral){
        Logger.debug("PUT /corrales/{} {}",corral.getId(), corral);
        oadCorrales.save(corral);
        return corral;
    }
    
    @RequestMapping(value="/{id}",method=RequestMethod.DELETE)
    public Corrales borrarCorral(@PathVariable("id") Integer id){
        Logger.debug("DELETE /corrales {}",id);
        oadCorrales.delete(id);
        return new Corrales(id);
    }
    
    
}
