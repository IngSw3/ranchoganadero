/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi16.ranchoganadero.Controllers;

import java.util.List;
import mx.lania.rysi16.ranchoganadero.entidades.RegistroEntradas;
import mx.lania.rysi16.ranchoganadero.oad.oadRegistro;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Lucy
 */
@RestController
@RequestMapping("/registro")
public class RegistroEntradasController {
    private static org.slf4j.Logger Logger = LoggerFactory.getLogger(RegistroEntradasController.class);
    
    @Autowired
    oadRegistro oadRegistro;
   
    @RequestMapping("/registro") //Metodo get que viene por default por eso no se pone
   // @GetMapping("/animal") //Metodo get que viene por default por eso no se pone
    public List<RegistroEntradas> getRegistroEntradas(){
        Logger.debug("GET /raza");
        return oadRegistro.findAll();
    }
    

   @RequestMapping(value="",method=RequestMethod.POST)
    public RegistroEntradas guardarRegistro(@RequestBody RegistroEntradas registro){
        Logger.debug("POST /registro {}",registro);
        oadRegistro.save(registro);
        return  registro;
    }
    
      @RequestMapping(value="/{id}",method=RequestMethod.PUT)
    public RegistroEntradas actualizarRegistro(@RequestBody RegistroEntradas registro){
        Logger.debug("PUT /ordenes/{} {}",registro.getIdRegistro(), registro);
        oadRegistro.save(registro);
        return registro;
    }
    
    @RequestMapping(value="/{id}",method=RequestMethod.DELETE)
    public RegistroEntradas borrarRaza(@PathVariable("id") Integer id){
        Logger.debug("DELETE /registro {}",id);
        oadRegistro.delete(id);
        return new RegistroEntradas(id);
    }
}
