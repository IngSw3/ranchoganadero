/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi16.ranchoganadero.oad;

import java.util.List;
import mx.lania.rysi16.ranchoganadero.entidades.RegistroEntradas;

/**
 *
 * @author Lucy
 */
public interface oadRegistro {
    void insertar(RegistroEntradas registros);
    void eliminar(Integer id);
    void editar(Integer id);
    RegistroEntradas showRegistros(Integer id);
    List<RegistroEntradas> listRegistros();
    
    List<RegistroEntradas> buscarActivosPorNombre(String nombre);

    List<RegistroEntradas> buscarPorNombre(String nombre);
    
    List<RegistroEntradas> getActivos();
    
    RegistroEntradas getPorId(Integer id);

    public List<RegistroEntradas> findAll();

    public void save(RegistroEntradas registro);

    public void delete(Integer id);
}
