/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi16.ranchoganadero.Controllers;

import java.util.List;
import java.util.logging.Logger;
import mx.lania.rysi16.ranchoganadero.entidades.Animal;
import mx.lania.rysi16.ranchoganadero.oad.oadAnimal;
import org.slf4j.LoggerFactory;
import static org.slf4j.LoggerFactory.getLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Lucy
 */
@RestController
@RequestMapping("/animal")
public class AnimalController{

   private static org.slf4j.Logger Logger = LoggerFactory.getLogger(AnimalController.class);
    
    @Autowired
    oadAnimal oadAnimal;
   
    @RequestMapping("/animal") //Metodo get que viene por default por eso no se pone
   // @GetMapping("/animal") //Metodo get que viene por default por eso no se pone
    public List<Animal> getAnimal(){
        Logger.debug("GET /animales");
        return oadAnimal.findAll();
    }
    

   @RequestMapping(value="",method=RequestMethod.POST)
    public Animal guardarAnimal(@RequestBody Animal animal){
        Logger.debug("POST /animales {}",animal);
        oadAnimal.save(animal);
        return  animal;
    }
    
      @RequestMapping(value="/{id}",method=RequestMethod.PUT)
    public Animal actualizarAnimal(@RequestBody Animal animal){
        Logger.debug("PUT /animales/{} {}",animal.getIdAnimal(), animal);
        oadAnimal.save(animal);
        return animal;
    }
    
    @RequestMapping(value="/{id}",method=RequestMethod.DELETE)
    public Animal borrarAnimal(@PathVariable("id") Integer id){
        Logger.debug("DELETE /animales {}",id);
        oadAnimal.delete(id);
        return new Animal(id);
    }
    
   /* @RequestMapping("/{id}")
    public Animal getAnimalPorId(@PathVariable("id") Integer id){
        org.slf4j.Logger.debug("GET /animales/{} ", id.toString());
        return oadAnimal.findOne(id);
    }*/
    
}
