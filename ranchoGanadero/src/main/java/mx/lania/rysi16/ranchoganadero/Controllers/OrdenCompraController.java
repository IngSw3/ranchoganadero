/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi16.ranchoganadero.Controllers;

import java.util.List;
import mx.lania.rysi16.ranchoganadero.entidades.OrdenCompra;
import mx.lania.rysi16.ranchoganadero.oad.oadOrdenCompra;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Lucy
 */
@RestController
@RequestMapping("/ordenCompra")
public class OrdenCompraController {
    private static org.slf4j.Logger Logger = LoggerFactory.getLogger(OrdenCompraController.class);
    
    @Autowired
    oadOrdenCompra oadOrdenCompra;
   
    @RequestMapping("/corral") //Metodo get que viene por default por eso no se pone
   // @GetMapping("/animal") //Metodo get que viene por default por eso no se pone
    public List<OrdenCompra> getOrdenCompra(){
        Logger.debug("GET /corrales");
        return oadOrdenCompra.findAll();
    }
    

   @RequestMapping(value="",method=RequestMethod.POST)
    public OrdenCompra guardarOrdenCompra(@RequestBody OrdenCompra orden){
        Logger.debug("POST /orden {}",orden);
        oadOrdenCompra.save(orden);
        return  orden;
    }
    
      @RequestMapping(value="/{id}",method=RequestMethod.PUT)
    public OrdenCompra actualizarOrdenCompra(@RequestBody OrdenCompra orden){
        Logger.debug("PUT /ordenes/{} {}",orden.getIdOrden(), orden);
        oadOrdenCompra.save(orden);
        return orden;
    }
    
    @RequestMapping(value="/{id}",method=RequestMethod.DELETE)
    public OrdenCompra borrarOrdenCompra(@PathVariable("id") Integer id){
        Logger.debug("DELETE /corrales {}",id);
        oadOrdenCompra.delete(id);
        return new OrdenCompra(id);
    }
}
