/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.social;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lucy
 */
public class OrdenadorCumpleaniosTest {
    
    public OrdenadorCumpleaniosTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("BeforeClass");
    }
    
    static OrdenadorCumpleanios instance;
    @AfterClass
    public static void tearDownClass() {
        System.out.println("AfterClass");
        instance = new OrdenadorCumpleanios(); 
    }
    
    @Before
    public void setUp() {
        System.out.println("Before");
    }
    
    @After
    public void tearDown() {
        System.out.println("After");
    }

    /**
     * Test of ordenarFechasPorCumpleanios method, of class OrdenadorCumpleanios.
     */
    @Test//("timeout =") //Puede utilizar para pruebas de rendimiento locales
    public void testOrdenarFechasConNull() {
        System.out.println("testOrdenarFechasConNull");
        List<Date> fechasNacimiento = null;
        try{
            List<Date> result = instance.ordenarFechasPorCumpleanios(fechasNacimiento);
            fail("Se esperaba una excepción");
        }catch(Exception ex){
        }
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testOrdenarFechasConNull2() {
        System.out.println("testOrdenarFechasConNull2");
        List<Date> fechasNacimiento = null;
        List<Date> result = instance.ordenarFechasPorCumpleanios(fechasNacimiento);
           
    }
    
    @Test
    public void testOrdenarFechasListaVacia() {
        System.out.println("testOrdenarFechasListaVacia");
        List<Date> fechasNacimiento = new ArrayList<>();
        //OrdenadorCumpleanios instance = new OrdenadorCumpleanios(); se elimina y se pone en el before class
        List<Date> expResult = new ArrayList<>();
        List<Date> result = instance.ordenarFechasPorCumpleanios(fechasNacimiento);//Regresa null, ahora si va a fallar
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype."); 
    }
    
     @Test
    public void testOrdenarFechasSencillo() {
        System.out.println("testOrdenarFechasSencillo");
        List<Date> fechasNacimiento = new ArrayList<>();
        fechasNacimiento.add(new Date(66,3,5));//Revisar documentacion de la fecha
        fechasNacimiento.add(new Date(86,10,6));
        fechasNacimiento.add(new Date(78,1,7));
        
       // OrdenadorCumpleanios instance = new OrdenadorCumpleanios(); también se elimina ya que se declaro en el before class
        List<Date> expResult = new ArrayList<>();
        
        expResult.add(new Date(66,3,5));//Revisar documentacion de la fecha
        expResult.add(new Date(86,10,6));
        expResult.add(new Date(78,1,7));
        
        List<Date> result = instance.ordenarFechasPorCumpleanios(fechasNacimiento);//Regresa null, ahora si va a fallar
        assertEquals("Número incorrecto de resultados",3,result.size());
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    
}
